//
//  ViewController.swift
//  Assignment #2
//
//  Created by test on 23.08.2018.
//  Copyright © 2018 Maks. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
    
        bolsheeChislo()
        kvadratIkubChisla()
        chislaPoPoryadku()
        chisloDeliteley()
        sovershennoeChislo()
    }
    
    func bolsheeChislo() {
        let a = 40
        let b = 20
        if (a > b) {
            print("Bolshee chislo a = \(a)")
        }
        if (a < b) {
            print("Bolshee chislo b = \(b)")
        }
    }
    
    func kvadratIkubChisla() {
        let c = 4
        let kvadrat = c * c
        let kub = kvadrat * c
        print("Kvadrat chisla c = \(kvadrat)")
        print("Kub chisla c = \(kub)")
    }
    
    func chislaPoPoryadku() {
        var d = 3
        print("chislaPoPoryadku")
        for _ in 0...d {
            d -= 1
            print("\(d + 1) \((d + 1) - d)")
        }
        
    }
    
    func chisloDeliteley() {
        let a = 6
        print("chisloDeliteley")
        for b in 1..<a {
            if a % b == 0 {
                print(b)
            }
        }
    }
    
    func sovershennoeChislo() {
        let a = 28
        print("sovershennoeChislo")
        for b in 1..<a {
            if a % b == 0 {
                print(b)
            }
        }
    }
    
}
